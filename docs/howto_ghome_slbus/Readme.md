![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

This documentation explains How to Enable Google Home Action for products based upon SL-BUS technology.

### Configuration Checkpoints

We have provided [SmartLivingConfigurator](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving_configurator) app. This app is generally used by our system integrators or service engineer to configure the products to make them ready for end user.

Please note that this Google Home Action reads the name programmed here and will be used for detection of particular device, so keep the group, node, scene names simple english words. This will make it easy for Google Home to address your installated devices for voice assistance.

### Pre-requisite

Before Starting this process please check all below check points -

1. Make sure SL-BUS devices are properly installed, configured and working for the basic connectivity.
2. Make sure SL-BUS devices are connected to the internet through a router, please note that Google Home control will only work if the devices are having internet connectivity.
3. Make sure all the devices are registered to a valid end user, and they are working properly from any SL-BUS Technology based Android or iOS app with cloud login (Connect Via cloud) interface.
4. Make sure the end user already have google account, if not make sure you should creates the same.
5. Download Google Home App from play store and login to it through your google's credentails.

### Steps to enable Google Home Action Support

* Step 1: Go to Google Home app in your phone

<img src="./images/01_ghome.jpg" width=50%>

* Step 2: Click on Get started to create Home (Skip stpe 2 and 3 if Home is already created)

<img src="./images/02_ghome.jpg" width=50%>

* Step 3: Enter Home nickname and Home Address (optional)

<img src="./images/03_ghome.jpg" width=50%>

* Step 4: After succesfully creating the Home, click on "not now", it will take to your Home page.

<img src="./images/04_ghome.jpg" width=50%>

* Step 5: Click on "+" icon on top left corner.

<img src="./images/05_ghome.jpg" width=50%>

* Step 6: Click on "Set up device".

<img src="./images/06_ghome.jpg" width=50%>

* Step 7: Click on "Have something already set up?" under Works with Google.

<img src="./images/07_ghome.jpg" width=50%>

* Step 8: Click on search icon to search for action. In the search window type "SLBUS" text and then search for action. You will find the Google Home Action "SLBUS Technology" that support these product range. In case if you are interested to use action for other SL-BUS Alliance member, then you can type respectiv name (for ex: SmartLiving, OSUM, etc...)

<img src="./images/09_ghome.jpg" width=50%>

* Step 10: Click on searched action name (for ex: "SLBUS Technology")

<img src="./images/10_ghome.jpg" width=50%>

* Step 11: Since this action needs account linking, you will be pointed to link your VDCloud account to your Google Home account.

<img src="./images/11_ghome.jpg" width=50%>

* Step 12: Here you should enter “VDCloud login name” as a username. (VDCloud login details will be provided by System Integrator to the end user - Generally it is email id of end user used during VDCloud login creation for end user). Then enter your VDCloud login password, and click on “login” button. After click you will get other screen but not devices list, you have to wait here for 20 secondsto get the device list.

<img src="./images/12_ghome.jpg" width=50%>

* Step 13: If your login credentials entered in earlier step are correct, then this screen will appear, this indicates that you have successfully configured SL-BUS Technology action for your Google Home Account and all online configured devices will get displayed. If still all your devices not get dispalyed here, then Re-power on all your SL-BUS devices, wait for 50-60 seconds and follow the procedure from Step 8 again.

<img src="./images/13_ghome.jpg" width=50%>

* Step 14: Now select the devices and click on "ADD TO A ROOM"

<img src="./images/14_ghome.jpg" width=50%>

* Step 15: Select room from list or Create New Room to add Device and Click on "Next"

<img src="./images/15_ghome.jpg" width=50%>

* Step 16: In this way you can add all the discovered devices in the sepctive GoogleHome rooms. Once addition of all Devices to Rooms is finished, then click on "Done"

<img src="./images/16_ghome.jpg" width=50%>

* Step 17: Your home page of Google Home App will get disaplayed and you are ready to control them through voice command as explained in below table or through Google Home App. If you change node/scene/group names, you will need to unlink and again re-linking the action for SL-BUS Technology based devices. If you find any device oflline or not reachable, you dont need to re-link the action again. In such case you can try again or after some time.

<img src="./images/17_ghome.jpg" width=50%>

### Known Issues: Incomplete Control for few Devices

1. Scene Control: At present scenes are disabled since there is issue (Not visible in the app), this will be addressed latter.
2. Fan Control: At present only on/off control is visible in the Google App for SL-BUS Product, whereas fan is operational for speed control from voice command. This is problem from Google side (Issue already raised to google).

Google Assistant Voice commands for SL-BUS Technology based device control

### Google Assistant Voice commands for SL-BUS Technology based device control

| S.N. | Product | Function | Voice command |
| :----: | :-----: | :-----: | :------: |
| 1 | SL-WSW-2CH-RL | To Turn On | OK Google, Turn on the "configred device name" |
| 2 | SL-WSW-2CH-RL | To Turn Off | OK Google, Turn off the "configred device name" |
| 3 | SL-WSW-2CH-TR | To Turn On | OK Google, Turn on the "configred device name" |
| 4 | SL-WSW-2CH-TR | To Turn Off | OK Google, Turn off the "configred device name" |
| 5 | SL-WSW-2CH-TR | To set dimming level | OK Google, Set the "configred device name" brightness to 25% |
| 6 | SL-WSW-2CH-TR(Fan) | To set fan speed | OK Google, Set the "configred device name" speed to 70% |
| 7 | SL-WSW-2CH-TR(Fan) | To set low fan speed(25%) | OK Google, Set the "configred device name" to low speed |
| 8 | SL-WSW-2CH-TR(Fan) | To set medium fan speed(50%) | OK Google, Set the "configred device name" medium speed |
| 9 | SL-WSW-2CH-TR(Fan) | To set high fan speed(75%) | OK Google, Set the "configred device name" high speed |
| 10 | SL-WCRC-STD | To open a curtain | OK Google, Open the "configred device name"  |
| 11 | SL-WCRC-STD | To close a curtain | OK Google, Close the "configred device name"  |
| 12 | SL-WCRC-STD | To open a curtain to a specific position| OK Google, Open the "configred device name" to 60% |
| 13 | SL-WRGBC-DCPWM | To Turn On | OK Google, Turn on the "configred device name" |
| 14 | SL-WRGBC-DCPWM | To Turn Off | OK Google, Turn off the "configred device name" |
| 15 | SL-WRGBC-DCPWM | To Set Color | OK Google, Set the "configred device name" to "desired color name" color |
| 16 | SL-WRGBC-DCPWM | To set dimming level | OK Google, Set the "configred device name" brightness to 75% |
| 17 | SL-WLIGHT-WCT | To Turn On | OK Google, Turn on the "configred device name" |
| 18 | SL-WLIGHT-WCT | To Turn Off | OK Google, Turn off the "configred device name" |
| 19 | SL-WLIGHT-WCT | To Set Color | OK Google, Set the "configred device name" to White |
| 20 | SL-WLIGHT-WCT | To Set Color Temperature | OK Google, Change the "configred device name" to 4000 kelvin |

### Modifying Configuration for Google Home

Let's say you have functional SL-BUS devices system with [SmartLiving](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving) UserApps as well as Google Home control. Now for some need you need to change the installation configuration. For ex: Customer has requested to change the "Lamp" node in the group "Hall" to "Tablelamp".

For this need for sure you will have to use  [SmartLivingConfigurator](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving_configurator) App and do the chnage. (of couse this setting is done by service engineer or system integrator). If you have to do such change you will need to follow below steps to get the change sucessfully reflected in the Google Home control.

1. First identify the change and using  [SmartLivingConfigurator](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving_configurator) App do the change.
2. Then using [SmartLiving](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving) UserApps validate the chnage is done properly.
3. Login from cloud in the app and check the changed name is reflected for cloud login as well.
4. Then go to Google Home App and Click on “+” icon on top left corner.

<img src="./images/05_ghome.jpg" width=50%>

6. Click on "Set up device".

<img src="./images/06_ghome.jpg" width=50%>

7. Click on "Have something already set up?" under Works with Google.

<img src="./images/07_ghome.jpg" width=50%>

8. You will find "SLBUS Technology" action in the "Linked services" list, click on it.

<img src="./images/18_ghome.jpg" width=50%>

9. Select option "Reconnect account" and follow the Steps from 12 to 17 from "Steps to enable Google Home Action Support" section.

### Support

If you need any further help, Please contact our [Support email](mailto:support@vadactro.org.in) on the email or you can raise the support ticket on our [VADACTRO_Help_Desk](https://vadactro.org.in/support/help) tool.

#### Enjoy Using Google Home voice and app control experience for SL-BUS Technology based Products..

***Last Updated: On 16th Dec 2020***

