![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

This documentation explains How to Enable SL-BUS Technology Skill for Range of products based upon this technology.

### Configuration Checkpoints

We have provided [SmartLivingConfigurator](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving_configurator) app. This app is generally used by our system integrators or service engineer to configure the products to make them ready for end user. to make SL-BUS Technology based system compatible with Alexa voice command service, you need to be bit carefull while donfiguring certina parameters as below

1. Do not use special characters liek %, $, @ while defining any name of the group, node or scene for your installation.
2. Please note that this Smarthome Skill reads the name programmed here and will be used for detection of particular device, so keep the group, node, scene names simple english words. This will make it easy for alexa to address your installated devices
3. Do not configure "device name" with any special characters   

### Pre-requisite

Before Starting this process please check all below check points -

1. Make sure your SL-BUS devices are properly installed, configured and working for the basic connectivity. 
2. Make sure SL-BUS devices are connected to the internet through a router, please note that Alexa control only works if the devices are having internet connectivity.
3. Make sure all the devices are registered to a valid end user, and they are working properly from SmartLiving Android or iOS app with cloud login (Connect Via cloud) interface.
4. Make sure the end user already have amazon account, if not make sure he/she creates the same.
5. Make sure end user has Alexa Voice Device (ex: Echo dot), powered on and configured to his amazon account and is functional for standard Alexa use.

### Steps to enable Alexa Smart Home Skill Support

* Step 1: Go to Alexa app in your phone

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation
/uploads/70b972b8a3437f268446a997c0ed43cb/Slide1.PNG
" width=50%>

* Step 2: Go to Menu->Skills Section

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/77ca5b982cf83a49121f48498eb0a42e/Slide2.PNG" width=50%>

* Step 3: In the search window, type in "SL-BUS" text, and then search the skill

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/9ebc9139a3b5eaaf24d9e29e30bf99cb/Slide3.PNG" width=50%>

* Step 4: As a result of Search operation you will find the skills that support these product range, at present there are two types of skill that support SL-BUSProducts, click on "SL-BUS Technology" Skill

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/043da7ee5c78f929e9b6b5394b6830be/Slide4.PNG" width=50%>

* Step 5: Click on "Enable" Button to add SL-BUS products to your Amazon account.

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/04426c295f77bedeb0824ecd77bb205e/Slide5.PNG" width=50%>

* Step 6: Since this skill needs account linking, you will be pointed to link your VADACTRO account to your amazon account.

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/f6bf4d80f530bf3b9543817f46846467/Slide6.PNG" width=50%>

* Step 7: Here you should enter end users "cloud login name" as a username. (VADACTRO cloud login details will be provided by System Integrator to the end user - Generally it is email id of end user used during VADACTRO cloud login creation for end user). Then enter your VADACTRO cloud login password, and click on "login" button.

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/9a802a53d162dece6975922daa702665/Slide7.PNG" width=50%>

* Step 8:  If your login credentials entered in earlier step are correct, then this screen will appear, this indicates that you have successfully configured SL-BUS Technology skill for your amazon account
  
<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/a962bfcb323500c766dca8bf54d34334/Slide8.PNG" width=50%>

* Step 9: When you close the successful linking message, you will be prompted to discover the devices, you can discover all your configured devices by clicking "discover button" as shown below.

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/8f439a285883409333a762ff3ff28fe4/Slide9.PNG" width=50%>

OR you can simple say "Alexa, discover devices", this voice command will do the same task for you.

Device Discovery in progress...

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/d87760973144b3b1b72dd12c5a58fc8b/Slide10.PNG" width=50%>

Following things may happens during device discovery

1. Since SL-BUS devices are online and configured, all pre-configured devices will be get discovered.
2. When you are executing this command at very first time, it is very likely your device discovery may fail, in such case wait for 30-40 seconds and again start device discovery.
3. If still your device discovery fails, then Re-power on all your SL-BUS devices, wait for 50-60 seconds and re-run the discovery


* Step 10: As a result of successful discovery you should get the list of devices in your Alesa app as shown below, make sure all the devices are present there, if not re-run the discovery.

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/dcb8db84ab43aa83797dceca5220ea05/Slide11.PNG" width=50%>

Step 11: Check all the pre-programmed scenes are also discovered by Alexa bo clicking "scenes" in the Alexa app.

<img src="https://gitlab.com/VDDOCS-PUBLIC/documentation/uploads/41cdbcb9f44bd995c086ccd8e1232c6d/Slide12.PNG" width=50%>

Step 11: Once you get all the devices and nodes in your Alexa, app you are ready to control them through voice command as explained in below table. You don't need to re-run device discovery in future if you are not changing the installed system. If you find any device oflline or not reachable, you dont need to execute device discovery. 

### Voice commands for SL-BUS devices

| S.N. | Product | Function | Voice command |
| :----: | :-----: | :-----: | :------: |
| 1 | SL-WSW-2CH-RL | To Turn On | Alexa, Turn on <configred device name> |
| 2 | SL-WSW-2CH-RL | To Turn Off | Alexa, Turn off <configred device name> |
| 3 | SL-WSW-2CH-TR | To Turn On | Alexa, Turn on <configred device name> |
| 4 | SL-WSW-2CH-TR | To Turn Off | Alexa, Turn off <configred device name> |
| 5 | SL-WSW-2CH-TR | To set dimming level | Alexa, Set <configred device name> to 30% |
| 6 | SL-WSW-2CH-TR(Fan) | To set fan speed | Alexa, Set <configred device name> to 40% |
| 7 | SL-WCRC-STD | To open a curtain | Alexa, Turn on <configred device name>  |
| 8 | SL-WCRC-STD | To close a curtain | Alexa, Turn off <configred device name>  |
| 9 | SL-WC-SCN | To invoke a scene | Alexa, Turn off <configred scene name> (Please not, scene works on for "turn on" or "set" commands |
| 10 | SL-WRGBC-DCPWM | To Turn On | Alexa, Turn on <configred device name> |
| 11 | SL-WRGBC-DCPWM | To Turn Off | Alexa, Turn off <configred device name> |
| 12 | SL-WRGBC-DCPWM | To Set Color | Alexa, Set <configred device name> to <color> |

### Modifying Configuration for alexa

Let's say you have functional SL-BUS devices system with [SmartLiving](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving) UserApps as well as Alexa voice control. Now for some need you need to change the installation configuration. For ex: Customer has requested to change the "Lamp" node in the group "Hall" to "Tablelamp".

For this need for sure you will have to use  [SmartLivingConfigurator](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving_configurator) App and do the chnage. (of couse this setting is done by service engineer or system integrator). If you have to do such change you will need to follow below steps to get the change sucessfully reflected in the Alexa voice control.

1. First identify the change and using  [SmartLivingConfigurator](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving_configurator) App do the change.
2. Then using [SmartLiving](https://play.google.com/store/apps/details?id=in.org.vadactro.smartliving) UserApps validate the chnage is done properly.
3. Login from cloud in the app and check the changed name is reflected for cloud login as well.
4. Then go to alexa.amaxon.com on the PC or laptop, login this webpage with customer credentials, or you can ask customer to do this step since this is his private domain. ***Please Node: This site should be only opened from PC or laptop, from mobile app below process will not work.***
4. When the alexa web UI is logged in, go to Smart Home -> Devices as shown below

<img src="./alexa_ug_step20.png" width=100%>

5. As a result you will see list of all devices discovered earlier by the system, now scroll down the device list and at the end you will see "Forget All" button, hit this button, with this alexa will no longer have old list of devices discovered earlier

<img src="./alexa_ug_step21.png" width=100%>

5. Now you re-discover the device, either through app or using Alexa device (For ex: Echo dot), you will see the change is reflrected as expected. Check first the change that you did (for ex: Say - Alexa, Turn on tablelamp - In this example). Then check all other older commands as well.

**NOTE: If you skill #4 step and rediscover devices, the new additional devices will be created for the changed names, thus you will end up with two device by similar name and then when you will issue a voice command, alexa will always confuse since she will find two or more simlar device of the same name.**

### Support

If you need any further help, Please contact our [Support channel](http://smartliving.net.in/index.html#Main-Services-Page) on the phone or you can raise the support ticket on our [Online Help Desk](mailto:support@vadactro.zohodesk.com) tool.

#### Enjoy Using Alexa voice control experience with SL-BUS Technology based Products..

***Last Updated: On 19th Jan 2020***

