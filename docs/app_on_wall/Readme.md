![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

App-On-Wall allows you to connect phone or raspberry pi to SL-Bus devices using LAN, allowing you consistant and reliable system to operate.

***NOTE:*** This feature is only available in App version 7.0.11 and above

App on wall using Android device
---

Using android device for App on wall feature has below advantages
* Strong processing power
* Easy to integrate

### Pre-requisite

Before Starting this process please check all below check points

- A connector to attach phone and wired network (eg ethernet).  
**Recommendation**: Use [6 in 1 connector](https://pibox.in/product/usb-c-hub-dock-pibox-india-6-in-1-aluminum-type-c-adapter-with-hdcp-support-4k-hdmi-port-ethernet-100mbps-rj45-port-usb-3-0-port-usb-c-power-delivery-macbook-samsung-usb-c-devices/), with this device you can charge your device, connect data cable and connect to network at the same time. Also there are additional USB ports for any other accessaries.

### Steps
1. Install the user app.
2. Disable your wifi connection and data pack on the device
3. Connect your device to a network using connector.  
<img src="./images/hardware.jpg" height="500">
4. Enable auto start app feature.  
    - Open the app's info setting  
    - Scroll down to option "Display over other apps" or "Appear on top" section.  
<img src="./images/ontop.jpg" height="500">  
    - Enable the feature.

App on wall using Raspberry Pi
---

Using Raspberry pi for App on Wall has below advantages
1. Latest android OS can be installed.
2. Larger screen sizes are available to use as display.

### Pre-requisite

Before Starting this process please check all below check points
1. Raspberry pi model
2. Display panel

Connect Rasberry pi and display panel as per vendors instruction.

### Steps
1. Visit [link](https://konstakang.com/devices/), and select your raspberry pi model.
2. Select proper build AOSP or LineageOS. Doownload the latest android OS available. I will use LineageOS 21 (Android 14) to demonstrate the process.  
**Important** The license for LineageOS and AOSP are [non-commercial](https://creativecommons.org/licenses/by-nc-sa/4.0/). With this documentation, VADACTRO is only educating on how to use these OS builds. 
**Declaration** VADACTRO is not reponsible for any licensing.
3. Download the zip file **[lineage-21.0-20240816-UNOFFICIAL-KonstaKANG-rpi4.zip](https://dlupload.com/filedetail/2099151521)**
4. Download image writer from official [site](https://www.raspberrypi.com/software/)  
<img src="./images/imager.png" width="300">
5. Follow the official [Raspberry Pi instructions](https://www.raspberrypi.org/documentation/computers/getting-started.html#installing-the-operating-system) for writing the image to the SD card.
6. Start the OS in raspberry pi and follow the displayed instruction to setup the device.
7. Open **Setting**->**System**->**Raspberry Pi settings**
8. Rotate the screen 90 degree from **Display rotation**. Also enable force rotate.  
<img src="./images/rasp_rotate.jpg" width="300">  
9. Follow the steps of **App on wall using Android**

#### Support

If you need any further help, Please contact our [Support channel](https://www.vadactro.org.in/support/help) or you can raise the support ticket on our [Online Help Desk](mailto:support@vadactro.org.in) email.
#### Enjoy Using SL-BUS Technology Products

***Last Updated: On 23rd Nov 2024***