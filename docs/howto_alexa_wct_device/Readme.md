![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

This documentation explains a process to configure Alexa voice control for white tunable lighting fixtures based upon IEC62386 specifications using SL-BUS Technology based alexa skill


### Why this is needed?

1. To control on/off using voice commands.
2. To control dimming using voice commands.
3. To control color temperature of a fixture using voice commands.

### Pre-requisite

Before Starting this process please check all below check points -

1. It is assumed that you already have pre-installed SL-BUS Technology based products with white tunable fixtures in it.
2. Make sure pre-installed system are properly installed, configured to your home router and router is having internet. 
3. Make sure your system works well through cloud login with SL-BUS Technology Apps
3. Make sure your system is pre configured with your Alexa App, respective skill is enabled and device discovery is already done

### Controlling color tunable fixtures using voice commands

* Utterances used with Alexa are (here Lamp is a name of Node)

#### To set Color Temperature Warm (3000 K)
* Alexa, make the Lamp warm (OR)
* Alexa, set the Lamp to warm

#### To set Color Temperature Cool (7000 K)
* Alexa, make the Lamp cool (OR)
* Alexa, set the Lamp to cool

#### To set Color Temperature to Daylight (5500 K)
* Alexa, make the Lamp daylight (OR)
* Alexa, set the Lamp to daylight

#### To set Color Temperature to White (4000 K)
* Alexa, make the Lamp white
* Alexa, set the Lamp to white.

#### To Dim
* Alexa, set the Lamp to 30%
		
#### To Make Lamp On/Off
* Alexa, turn on the Lamp
* Alexa, turn off the Lamp

### Support

If you need any further help, Please contact our [Support channel](https://www.vadactro.org.in/support/help) or you can raise the support ticket on our [Online Help Desk](mailto:support@vadactro.org.in) email.

#### Enjoy Using SL-BUS Technology Products

***Last Updated: On 22nd May 2020***

