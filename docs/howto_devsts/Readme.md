![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

This documentation explains a device status reporting feature and it's usage for SL-BUS device

Please NOTE: This feature is only available in the firmware released after March 2024 (i.e. Version 6.0.20240329 onwards).

### What was a problem?

In earlier release (older than 6.0.20240329), if UI needs to display current state (on/off/dim level) for any SL-BUS Technology based node (a device that gets controlled, for ex: Switch, Curtain or Lamp), that needed to be polled before display. When the state of such node is changed by any other means (for ex: rule engine set for sensor or wall scene control, etc) then UI do not know which device state has changed, and in such case there was no option than polling the state of the node in UI.

### Why this is needed?

Though the above problem can be manged by polling status, but in certain cases it is not possible to poll the node state, also it creates extra overhead in the UI making it slow. To over come such issues, a new feature is introduced in the SL-BUS Technology stack called as "Device Status Reporting".

### How it works?

In SL-BUS Technology device network topology there is one communicator which communicates with UI (apps) through SL-BUS APIs (JSONised http get/post command) mainly used to control, configure or status reporting. There could be many nodes (max 64) under one such communicator that is called as island, and there could be one or many groups in one lsand, and each group may have nodes configured in it, those nodes will be exposed in the UI and that may need a device status information on change. So finally in one particular island either few of the nodes or all of the nodes may need device status to be reported as per custom configuration. 

NOTE: The nodes only configured under groups will be enabled to report their current status either on change (which is immediate within 3 seconds after change) or periodically after each 5 minutes.

<img src="./images/SLBUS_DevStsReporting_MQTT_UDP.png" width=100%>

- Why not all of the nodes are enabled by default?

One SL-BUS island can have maximum 64 nodes and each of these may not be enabled to be displayed in the UI, so posting there status is useless. The device status is an extra information transmitted from an island to the UI over a network, to keep the network traffic minimum that helps to improve overall system performance.

### Modes of Device status Reporting

There are two modes of reporting provided

1. Using MQTT Protocol: This is recommended mode of reporting, it provides precise filtered information as per need but has internet dependency since status information travels via the VDCloud to the UI
2. Using UDP Protocol: This is alternate mode of reporting, once can use this method when internet is not available, it provides the information from all (unfiltered) the SL-BUS device communicators available on that particular local WiFi network. Secondly UDP information is broadcasted hence it's delivery can not be confirmed on the complex WiFi network (specially where repeaters are used).

You may use any one of this method that suites for your need as explained below.

### MQTT Based Device Status Reporting

1. Make sure firmware on your SL-BUS Technology device is no older than 6.0.20240329
    
2. In this mode device reports device status JSON object to the cloud in case if the state of any node is changed as a part of action data, as well it posts status on regular interval as a part of periodic data. This feature is supported from device side and it posts the status, UI device needs to subscribes to the status topic that can receive the status information.

#### How to Subscribe a MQTT Status topic from Linux environment?

* To check status of individual device, execute command

mosquitto_sub -h mqtt.slbus.in -p 1883 -t "CID"/"UUID"/devsts

Here, to get status reporting of individual device, put Device UUID in command. 

* To check status of all devices, execute command

mosquitto_sub -h mqtt.slbus.in -p 1883 -t "CID"/+/devsts

NOTE: "CID" is Customer ID and "UUID" is unique device identification. Both "CID" as well "UUID" can be fetched using SL-BUS login API.

##### VDCloud will post messages for a Customer as below:

* Case 1 (Example). Messages outputs on change of state either from app or from panel keys or scene execution:

{"status":{"timestamp":1658919846,"status":"pass","msg":"action","uuid":"4530383630021457346f246f8945","islands":[{"bus_id":0,"nodes":[{"address":0,"value":"0%"},{"address":1,"value":"0%"},{"address":2,"value":"0%"},{"address":3,"value":"0%"}]}]}}
    
* Case 2 (Example). Messages outputs periodically after each 5 minutes to indicate power status as well as device is online.

{"status":{"timestamp":1658920147,"status":"pass","msg":"periodic_post","uuid":"4530383630021457346f246f8945","islands":[{"bus_id":0,"nodes":[{"address":0,"value":"0%"},{"address":1,"value":"0%"},{"address":2,"value":"0%"},{"address":3,"value":"0%"},{"address":4,"value":"0%"},{"address":5,"value":"0%"},{"address":6,"value":"0%"},{"address":7,"value":"0%"},{"address":8,"value":"0%"},{"address":9,"value":"0%"},{"address":10,"value":"0%"},{"address":11,"value":"0%"},{"address":12,"value":"0%"},{"address":13,"value":"0%"},{"address":14,"value":"0%"},{"address":15,"value":"0%"},{"address":16,"value":"0%"},{"address":17,"value":"0%"},{"address":18,"value":"0%"},{"address":19,"value":"0%"},{"address":20,"value":"0%"},{"address":21,"value":"0%"}]}]}}

### UDP Based Device Status Reporting

1. Make sure firmware on your SL-BUS Technology device is no older than 6.0.20240329

2. Make sure UDP server is enabled on the WiFi Communicator device

<img src="./images/EnableUDP_Server.jpeg" width=50%>
    
3. In this mode device broadcasts device status JSON object on the 3000 UDP port. The IP address used can be derived from the device ipaddress (for ex, if device IP address is x.y.z.120 then the UDP broadcast IP address will be x.y.z.255:3000.

#### How to test device status UDP packet receipt using wireshark?

1. Install wireshark tool in your desktop/laptop

2. Make sure your desktop or laptop is connected to the same router to which the WiFi Communicator is connected.

3. Open the tool, set filter as "udp.port==3000" and start capturing you will see the packets on this port coming from WiFi communicator, if you fetch the packet details then you will find the header information JSON object having the the "devsts" composite object in it.

<img src="./images/wireshark_scnrrenshot.png" width=100%>

NOTE: only the devsts JSON object is useful from the udp broadcasted packet, rest other information you can discard.

* Case 1 (Example). Messages outputs on change of state either from app or from panel keys or scene execution:

{"header":{"uuid":"453038363002b682346f246f632d","rmacid":"d8:0d:17:54:ff:ea","mc200fw":"6.0.20240415","ftfs":"2.1.0","wififw":"2.4.8","time":1713169490,"clic":"DemoVersion","bus":0,"dname":"SL-WBSM-R5-IV3-632D","sname":"EU1564125405320","ipAddress":"192.168.2.166","groupList":[{"groupId":"Ga","groupName":"SmartLiving"}],"type":"SL-WBSM-R5-IV3"},"periodicdata":{"devsts":{"custid":"EU1564125405320","data":[{"ad":61,"vl":0}],"msg":"action"}}}

Of which only devsts JSON object has the needed information as below

{"devsts":{"custid":"EU1564125405320","data":[{"ad":61,"vl":0}],"msg":"action"}}

NOTE: "ad" represents node address whereas "vl" represent value, in MQTT message it is expanded, UDP packet is coming streight from device hence need to manage with these short names to manage packet transfer within limited buffer size.
    
* Case 2 (Example). Messages outputs periodically after each 5 minutes to indicate power status as well as device is online.

{"header":{"uuid":"453038363002b682346f246f632d","rmacid":"d8:0d:17:54:ff:ea","mc200fw":"6.0.20240415","ftfs":"2.1.0","wififw":"2.4.8","time":1713169791,"clic":"DemoVersion","bus":0,"dname":"SL-WBSM-R5-IV3-632D","sname":"EU1564125405320","ipAddress":"192.168.2.166","groupList":[{"groupId":"Ga","groupName":"SmartLiving"}],"type":"SL-WBSM-R5-IV3"},"periodicdata":{"devsts":{"custid":"EU1564125405320","data":[{"ad":59,"vl":0},{"ad":60,"vl":0},{"ad":61,"vl":0},{"ad":62,"vl":0},{"ad":63,"vl":0}],"msg":"periodic_post"}}}

Of which only devsts JSON object has the needed information as below

{"devsts":{"custid":"EU1564125405320","data":[{"ad":59,"vl":0},{"ad":60,"vl":0},{"ad":61,"vl":0},{"ad":62,"vl":0},{"ad":63,"vl":0}],"msg":"periodic_post"}}

### Support

If you need any further help, Please contact our [Support channel](https://www.vadactro.org.in/support/help) or you can raise the support ticket on our [Online Help Desk](mailto:support@vadactro.org.in) email.
#### Enjoy Using SL-BUS Technology Products

***Last Updated: On 29th Mar 2024***

