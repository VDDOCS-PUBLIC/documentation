![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

This documentation explains a process to registere a SL-BUS device to end user

Please NOTE: This task need to be performed by Product Support team, their system integrators or support engineers. Device registration is not for end user.

### Why this is needed?

1. It is mandatory to assign a device to a user before using it.
2. To get rid of "Unregistered Device" on the SmartApp UI
3. To enable better suport structure and after sales support
4. To configure a device on wired bus operation

### Pre-requisite

Before Starting this process please check all below check points -

1. Make sure your SmartLiving Home Automation devices are properly installed, configured to your home router and router is having internet. 
5. Make sure you yourself is registered SI and has SI Admin login credentials

### Steps to follow to do device registery

* Step 1: Locate the UUID on the device

This is printed on the device as QR code image, scan it and copy it to your safe place so that you can use it latter in this process

* Step 2: Login to SL-BUS Customers Management Portal

Got to [SL-BUS Customers Management Portal](https://customers.vadactro.org.in), login with your SI admin credentials


* Step 3: Create a End Users Account

If you have already created a user account earlier then skip this step

If this is first time then, create the user acount by grabbing information from end user, please note users email id and mobile no with country code will be be unique and cannot be repeated for multiple customers.

* Step 4: Assign a device to the user

Click on the check box for the user account to which you wish to add a device, and goto "Manage UUIDs" button

You will see the list of devices assigned to a particular use

Copy paste the UUID for the device which you captured earlier in this window in front of "Add UUIDs" data entry box and hit the add button.

The device will be assigned to the user, if UUIDs is valid and the message will be displayed on the UI

You can refresh this page to see the newly added device appears in the list.


### Support

If you need any further help, Please contact our [Support channel](https://www.vadactro.org.in/support/help) or you can raise the support ticket on our [Online Help Desk](mailto:support@vadactro.org.in) email.

#### Enjoy Using SL-BUS Technology Products

***Last Updated: On 22nd Jan 2020***

