![](http://vadactro.org.in/images/VD_logo_wm.png)

# Introduction

This documentation explains a process to execute Alexa voice control for TV or SettopBox for typical use case using SL-BUS Technology based Alexa skill and IR blaster product


### Why this is needed?

1. To control power on/off using voice commands.
2. To increase/decrease/mute volume using voice commands.
3. To change channel using voice commands.

### Pre-requisite

Before Starting this process please check all below check points -

1. It is assumed that you already have pre-installed SL-BUS Technology based products with white tunable fixtures in it.
2. Make sure pre-installed system are properly installed, configured to your home router and router is having internet. 
3. Make sure your system works well through cloud login with SL-BUS Technology Apps
4. Make sure your system is pre configured with your Alexa App, respective skill is enabled and device discovery is already done
5. Make sure support of SL-WIRB (IR Blaster) is already added and configured in your installation.

* To use it, program remote keys using SmartLiving Configurator App mentioned as in the following table

|Sr. No. |Remote Key|IR Code Position|
|-----|----|--------|
|1|0|230|
|2|1|231|
|3|2|232|
|4|3|233|
|5|4|234|
|6|5|235|
|7|6|236|
|8|7|237|
|9|8|238|
|10|9|239|
|11|Next Channel|240|
|12|Previous Channel|241|
|13|Volume Up|242|
|14|Volume Down|243|
|15|Mute|244|
|16|Power|245|

### Controlling TV or Set-Top-Box Operation using voice commands.

* Utterances used with Alexa are (here Living Room TV is a name of Node)

#### Voice Control Commands.
* Alexa, change channel to 200 on Living Room TV
* Alexa, next channel on Living Room TV  /  Alexa, channel up on Living Room TV
* Alexa, previous channel on Living Room TV /  Alexa, channel down on Living Room TV
* Alexa, volume up on Living Room TV (It will increase volume by 3 step)
* Alexa, volume Down on Living Room TV (It will decrease volume by 3 step)
* Alexa, increase volume by 1 on Living Room TV (range will be 1 to 5 Max) 
* Alexa, decrease volume by 1 on Living Room TV (range will be 1 to 5 Max)
* Alexa, mute Living Room TV
* Alexa, unmute Living Room TV
* Alexa, Turn On Living Room TV
* Alexa, Turn Off Living Room TV

### Support

If you need any further help, Please contact our [Support channel](https://www.vadactro.org.in/support/help) or you can raise the support ticket on our [Online Help Desk](mailto:support@vadactro.org.in) email.

#### Enjoy Using SL-BUS Technology Products

***Last Updated: On 22nd May 2020***

