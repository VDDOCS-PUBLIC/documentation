![](http://vadactro.org.in/images/VD_logo_wm.png)

# Documentation for VADACTRO Products

Thanks for trying out VADACTRO's SL-BUS Technology based products. This documentation provides all the information about VADACTRO products

Below are the detailed guides as per different applications, please use the relevant to your interest

###[HowTo Enable Google Home Action for SL-BUS Technology Based Device Control?](./docs/howto_ghome_slbus)

This documentation explains how to configure and enable SL-BUS Technology based Products for control from Google Home App and Google assistant based voice control.

###[SmartHome Skill "OSUM SmartLiving Home Automation" interface guide](./docs/alexa_ossl_shskill)

This documentation explains how to enable Alexa voice control support for SmartLiving products installation (old as well as new)

###[SmartHome Skill "SLBUS Technology" interface guide](./docs/alexa_slbus_shskill)

This documentation explains how to enable Alexa voice control support for Generic SL-BUS Technology based products

###[HowTo Register SL-BUS Device?](./docs/howto_register_device)

This documentation explains how to register a SL-BUS technology device to a particular end user by the SystemIntegrator

###[HowTo Control Tunable Lights from SL-BUS Alexa Skills?](./docs/howto_alexa_wct_device)

This documentation explains how to configure and control with alexa voice commands a white tunable fixtures for color as well as dimmin level using SL-BUS technology based alexa skills

###[HowTo Control TV/SettopBox from SL-BUS Alexa Skills using IR-Blaster Device?](./docs/howto_alexa_irb_device)

This documentation explains how to configure and control TV or SettopBox controls with alexa voice commands using SL-BUS technology based alexa skills and IR-Blaster device

###[Howto Use Device Status Reporting Feature for SL-BUS Technology Based Devices?](./docs/howto_devsts)

This documentation explains how to use Device Status Reporting feature from UI (apps or web or local UI) to report current node status changed outside of UI (i.e. from wall control or by anyother control than UI). This feature is avaialble for all SL-BUS technology based devices having firmware version higher than 6.0.20240329

### [App on the wall](./docs/app_on_wall)

This documentation explains how we can use Android phone or Raspberry PI on wired connection and enable the App on Wall feature. This is only available on App version v7.0.11 and above.
